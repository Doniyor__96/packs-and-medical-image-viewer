import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Router} from '@angular/router';
import {Patients} from '../shared/patients';
import {StudyService} from '../shared/services/study.service';

@Component({
  selector: 'app-study-list',
  templateUrl: './study-list.component.html',
  styleUrls: ['./study-list.component.scss']
})
export class StudyListComponent implements OnInit {
  loader = false;
  patients: Patients[] = [];

  constructor(private authService: AuthService,
              private studyService: StudyService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loader = true;
    this.studyService.getStudies().subscribe(next => {
      this.patients = next;
      this.loader = false;
      // console.log(this.patients);
    }, error => {
      console.log(error);
      this.loader = false;
    });
  }

  logout(): void {
    this.authService.logout();
    void this.router.navigate(['/', 'auth']);
  }


}
