import {Component, OnInit} from '@angular/core';
import {Patients} from '../shared/patients';
import {AuthService} from '../shared/services/auth.service';
import {StudyService} from '../shared/services/study.service';
import {Observable} from 'rxjs';
import {HttpClient, HttpEvent, HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-radiology',
  templateUrl: './radiology.component.html',
  styleUrls: ['./radiology.component.scss']
})
export class RadiologyComponent implements OnInit {
  loader = false;
  patients: Patients[] = [];
  selectedFiles: FileList;
  patientID = '';
  patientName = '';

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private studyService: StudyService) {
  }

  ngOnInit(): void {
    this.loader = true;
    this.studyService.getStudies().subscribe(next => {
      this.patients = next;
      this.loader = false;
      console.log(this.patients);
    }, error => {
      console.log(error);
      this.loader = false;
    });
  }

  uploadFile(): void {
    const formData: FormData = new FormData();
    formData.append('user', '3');
    for (let i = 0; i < this.selectedFiles.length; i++) {
      formData.append('dicomFile', this.selectedFiles[i], this.selectedFiles[i].name);
    }
    this.uploadFiles(this.patientID, formData)
      .subscribe((event: HttpEvent<{ key: string }>) => {
        if (event.type === HttpEventType.UploadProgress) {
          console.log(Math.round(event.loaded / (event.total || 0) * 100));
        } else if (event.type === HttpEventType.Response) {
          alert('success');
          console.log('event', event);
        }
      });
  }

  selectFiles($event: Event): void {
    this.selectedFiles = ($event.target as HTMLInputElement).files;
  }

  private uploadFiles(patientID: string, file: FormData): Observable<HttpEvent<object>> {
    return this.http.post('https://packs.polito.uz/api/v1/dicom/patients/' + patientID + '/add_dicom/', file, {
      reportProgress: true,
      observe: 'events'
    });
  }

  getId(id: string, firstname: string, lastname: string) {
    this.patientID = id;
    this.patientName = firstname + ' ' + lastname;
  }
}
