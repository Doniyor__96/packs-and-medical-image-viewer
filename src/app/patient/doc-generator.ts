import {AlignmentType, Document, HeadingLevel, LineRuleType, Paragraph, TabStopPosition, TabStopType, TextRun} from 'docx';

const PHONE_NUMBER = "+998(97)123-45-67";
// const PATIENT_ID = "45645642312315456132";
const EMAIL = "test@mail.ru";

const BIRTH_DATE = '02/12/1991';

const locale = 'ru-Ru';
const dateOptions = {
  day: 'numeric',
  year: 'numeric',
  month: 'long'
};

export class DocumentCreator {

  public create([experiences, diagnostics, skills, achivements, PATIENT_NAME, PATIENT_ID, toolDataList]): Document {
    return new Document({
      sections: [
        {
          children: [
            new Paragraph({
              text: 'Ўзбекистон Соғлиқни сақлаш Вазирлиги \n' +
                'Республика Онкология Илмий Маркази\n' +
                '(10017, Тошкент шахри, Фаробий кўчаси, 383 уй)\n',
              heading: HeadingLevel.HEADING_2,
              spacing: {
                after: 500
              }
            }),
            new Paragraph({
              alignment: AlignmentType.CENTER,
              text: 'МАЪЛУМОТНОМА №' + 1 + '\n',
              heading: HeadingLevel.HEADING_2,
              spacing: {
                line: 1000,
                lineRule: LineRuleType.AUTO,
              }
            }),
            // new Paragraph({
            //   text: PATIENT_NAME,
            //   heading: HeadingLevel.TITLE
            // }),
            // this.createContactInfo(PHONE_NUMBER, PATIENT_ID, EMAIL),
            this.createHeading(`Ушбу маълумотнома ${PATIENT_NAME}, ${BIRTH_DATE} йилда туғилган. ${new Intl.DateTimeFormat(locale, dateOptions).format()} куни рентген снимкадан ўтди ва куйидагилар маълум бўлди:`),
            ...diagnostics
              .map(diagnosis => {
                const arr: Paragraph[] = [];
                arr.push(
                  this.createInstitutionHeader(
                    diagnosis.diagnosisName,
                    `${diagnosis.date}`
                  )
                );
                // arr.push(
                //   this.createRoleText(
                //     `${diagnosis.fieldOfStudy} - ${diagnosis.degree}`
                //   )
                // );

                // const bulletPoints = this.splitParagraphIntoBullets(
                //   diagnosis.notes
                // );

                toolDataList.forEach(toolData => {
                  if (toolData.name === 'Probe') {
                    arr.push(this.createBullet(toolData.name + ' : ' + toolData.data.x + ' mm, ' + toolData.data.y));
                  } else {
                    arr.push(this.createBullet(toolData.name + ' : ' + toolData.data + ' mm'));
                  }
                });

                return arr;
              })
              .reduce((prev, curr) => prev.concat(curr), []),
            // this.createHeading("Experience"),
            // ...experiences
            //   .map(position => {
            //     const arr: Paragraph[] = [];
            //
            //     arr.push(
            //       this.createInstitutionHeader(
            //         position.company.name,
            //         this.createPositionDateText(
            //           position.startDate,
            //           position.endDate,
            //           position.isCurrent
            //         )
            //       )
            //     );
            //     arr.push(this.createRoleText(position.title));
            //
            //     const bulletPoints = this.splitParagraphIntoBullets(
            //       position.summary
            //     );
            //
            //     bulletPoints.forEach(bulletPoint => {
            //       arr.push(this.createBullet(bulletPoint));
            //     });
            //
            //     return arr;
            //   })
            //   .reduce((prev, curr) => prev.concat(curr), []),
            // this.createHeading("Skills, Achievements and Interests"),
            // this.createSubHeading("Skills"),
            // this.createSkillList(skills),
            // this.createSubHeading("Achievements"),
            // ...this.createAchivementsList(achivements),
            // this.createSubHeading("Interests"),
            // this.createInterests(
            //   "Programming, Technology, Music Production, Web Design, 3D Modelling, Dancing."
            // ),
            // this.createHeading("References"),
            // new Paragraph(
            //   "Dr. Dean Mohamedally Director of Postgraduate Studies Department of Computer Science, University College London Malet Place, Bloomsbury, London WC1E d.mohamedally@ucl.ac.uk"
            // ),
            // new Paragraph("More references upon request"),
            // new Paragraph({
            //   text:
            //     "This CV was generated in real-time based on my Linked-In profile from my personal website www.dolan.bio.",
            //   alignment: AlignmentType.CENTER
            // })
          ]
        }
      ]
    });
  }

  public createContactInfo(
    phoneNumber: string,
    patientID: string,
    email: string
  ): Paragraph {
    return new Paragraph({
      alignment: AlignmentType.CENTER,
      children: [
        new TextRun(
          `Mobile: ${phoneNumber} | Email: ${email} | ID: ${patientID}`
        ),
        new TextRun({
          text: "Turin Polytechnic University In Tashkent, kichik xalqa yo'li 17",
          break: 1
        })
      ]
    });
  }

  public createHeading(text: string): Paragraph {
    return new Paragraph({
      text: text,
      heading: HeadingLevel.HEADING_3,
      spacing: {
        after: 500
      },
      thematicBreak: true,
    });
  }

  public createSubHeading(text: string): Paragraph {
    return new Paragraph({
      text: text,
      heading: HeadingLevel.HEADING_2
    });
  }

  public createInstitutionHeader(
    info: string,
    dateText: string
  ): Paragraph {
    return new Paragraph({
      tabStops: [
        {
          type: TabStopType.RIGHT,
          position: TabStopPosition.MAX
        }
      ],
      children: [
        new TextRun({
          text: info,
          size: 24,
          bold: true
        }),
        new TextRun({
          text: `\t${dateText}`,
          size: 24,
          bold: true
        })
      ]
    });
  }

  public createRoleText(roleText: string): Paragraph {
    return new Paragraph({
      children: [
        new TextRun({
          text: roleText,
          italics: true
        })
      ]
    });
  }

  public createBullet(text: string): Paragraph {
    return new Paragraph({
      bullet: {
        level: 0
      },
      children: [
        new TextRun({
          text: text,
          font: {
            name: "Monospace",
          },
          size: 22
        }),
      ],
    });
  }
  public createSkillList(skills: any[]): Paragraph {
    return new Paragraph({
      children: [new TextRun(skills.map(skill => skill.name).join(", ") + ".")]
    });
  }

  // tslint:disable-next-line:no-any
  public createAchivementsList(achivements: any[]): Paragraph[] {
    return achivements.map(
      achievement =>
        new Paragraph({
          text: achievement.name,
          bullet: {
            level: 0
          }
        })
    );
  }

  public createInterests(interests: string): Paragraph {
    return new Paragraph({
      children: [new TextRun(interests)]
    });
  }

  public splitParagraphIntoBullets(text: string): string[] {
    return text.split("\n\n");
  }

  // tslint:disable-next-line:no-any
  public createPositionDateText(
    startDate: any,
    endDate: any,
    isCurrent: boolean
  ): string {
    const startDateText =
      this.getMonthFromInt(startDate.month) + ". " + startDate.year;
    const endDateText = isCurrent
      ? "Present"
      : `${this.getMonthFromInt(endDate.month)}. ${endDate.year}`;

    return `${startDateText} - ${endDateText}`;
  }

  public getMonthFromInt(value: number): string {
    switch (value) {
      case 1:
        return "Jan";
      case 2:
        return "Feb";
      case 3:
        return "Mar";
      case 4:
        return "Apr";
      case 5:
        return "May";
      case 6:
        return "Jun";
      case 7:
        return "Jul";
      case 8:
        return "Aug";
      case 9:
        return "Sept";
      case 10:
        return "Oct";
      case 11:
        return "Nov";
      case 12:
        return "Dec";
      default:
        return "N/A";
    }
  }
}
